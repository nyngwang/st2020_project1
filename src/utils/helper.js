
let sorting = (array) => {
    // in-place sorting
    let length = array.length;
    for (let i = 1; i <= length-1; i++)
        for (let j = 0; j <= length-1-i; j++)
            if (array[j] > array[j+1]) {
                let temp = array[j];
                array[j] = array[j+1];
                array[j+1] = temp;
            }
    return array;
}

let compare = (a, b) => {
    let result = a["PM2.5"]-b["PM2.5"];
    if (result === 0) return 0;
    if (result > 0) return 1;
    return -1;
}

let average = (nums) => {
    let length = nums.length;
    let total = 0;
    for (let i = 0; i < length; i++) {
        total += nums[i];
    }
    let result = total / length;
    result = Math.round((result + Number.EPSILON)*100) / 100
	return result;
}

// let test = [1.1, 4, 34.277, 54, 989, 110.112];
// console.log(sorting(test));
// console.log(test);
// console.log(average(test));
// console.log(compare(1,2));

module.exports = {
    sorting,
    compare,
    average
}