const _ = require('lodash');
const chai = require("chai");
const expect = chai.expect; 


test('Should reverse an array', () => {
    const array = [1, 2, 3];
    let reversedArr = _.reverse(array);
    expect(reversedArr).to.eql([3, 2, 1]);
})

//1
test('Should find the last inactive user', () => {
    let users = [
        { 'user': 'barney',  'active': true },
        { 'user': 'fred',    'active': false },
        { 'user': 'pebbles', 'active': false }
    ];
    let indexLastNotActiveUser = _.findLastIndex(users, {'active': false});
    expect(indexLastNotActiveUser).to.equal(2);
})

test('Should find the last index of target from given string-int-mixed array', () => {
    const array = [0, "8", 9, "2", "2", "0", 9, "9"];
    let indexLastString9 = _.findLastIndex(array, (o) => { return o === "9";});
    expect(indexLastString9).to.equals(7);
})

//2
test('Should find the index of first occurrence from the sorted array', () => {
    const array = [0, 8, 9, 22, 99, 666, 666];
    let indexFirst666 = _.sortedIndexOf(array, 666);
    expect(indexFirst666).to.equal(5);
})

//3
test('Should create a duplicate-free version of the given array', () => {
    const array = [0, 8, 9, 2, 2, 0, 9, 9];
    let arrayUniq = _.uniq(array);
    expect(arrayUniq).to.eql([0,8,9,2]);
});

//4
test('Should create a duplicate-free version of the given array by some criterion,' +
    'and keep original order', () => {
    let users = [
        { 'user': 'barney',  'active': true },
        { 'user': 'fred',    'active': false },
        { 'user': 'pebbles', 'active': false }
    ];
    let arrayUniq = _.uniqBy(users, 'active');
    expect(arrayUniq).to.eql([{ 'user': 'barney',  'active': true }, { 'user': 'fred',    'active': false }]);
})

//5
test('Should unzip the given array', () => {
    const array = [[0, 9, 20], [8, 2, 99]];
    let arrayUnzipped = _.unzip(array);
    expect(arrayUnzipped).to.eql([[0, 8], [9, 2], [20, 99]]);
})

//6
test('Should unzip the given array, and sum-up each element array in the result', () => {
    const array = [[0, 9, 20], [8, 2, 99]];
    let arrayUnzippedWithAdd = _.unzipWith(array, _.add);
    expect(arrayUnzippedWithAdd).to.eql([8, 11, 119]);
})

//7
test('Should preclude given element in the array', () => {
    const array = [0, 8, 9, 2, 2, 0, 9, 9];
    let array9 = _.without(array, 0, 8, 2);
    expect(array9).to.eql([9, 9, 9]);
})

//8
test('Should create a new array that is the symmetric difference of the two.', () => {
    const array1 = [0, 8, 9, 2];
    const array2 = [22, 0, 9, 9];
    let arrayXor = _.xor(array1, array2);
    expect(arrayXor).to.eql([8, 2, 22]);
})

//9
test('Should create a new array that is the symmetric difference of the two.', () => {
    const array1 = [{'user': 'a', 'id': 0}, {'user': 'b', 'id': 8}, {'user': 'c', 'id': 9}];
    const array2 = [{'user': 'd', 'id': 22}, {'user': 'e', 'id': 0}, {'user': 'f', 'id': 99}];
    let arrayXorBy = _.xorBy(array1, array2, 'id');
    expect(arrayXorBy).to.eql([{'user': 'b', 'id': 8}, {'user': 'c', 'id': 9}, {'user': 'd', 'id': 22},
        {'user': 'f', 'id': 99}]);
})

//10
test('Should create a element-wise grouped array from given two.', () => {
    let arrayZipped = _.zip([0, 22],[8, 0], [9, 99]);
    expect(arrayZipped).to.eql([[0, 8, 9], [22, 0, 99]]);
})
